<?php
require_once 'lib/core.php';
require_once 'lib/manager.php';
$database = $_GET['database'];
$table = $_GET['table'];
$manager = new TableRelationManager();
if (isset($_POST['original_field'])) {
	$manager->addRelation($_POST);
}
if (isset($_POST['remove_relation'])) {
	$manager->removeRelation($_POST);
}
$tables = $manager->getDatabaseTables();
$columns = $manager->getTableColumns($table);
$relations = $manager->getExistingRelations($database, $table);
$foreign = array();
$master = array();
$relatedFields = array();
foreach ($relations as $relation) {
	if ($relation['foreign_table'] == $table) {
		$master[] = $relation;
		$relatedFields[] = $relation['foreign_field'];
	} else {
		$foreign[] = $relation;
		$relatedFields[] = $relation['master_field'];
	}
}

$styles = array('img/settings.css');
require_once 'inc/header.php';
?>
	<h1><a href="./?database=<?=$database?>"><?=$database?></a>.<a href="view.php?database=<?=$database?>&table=<?=$table?>"><?=$table?></a> table relations:</h1>
	<form action="<?=$_SERVER['REQUEST_URI']?>" method="post" name="add_relation_form" class="relations-editor">
		<div class="foreign-tables">
			<table class="column-list">
				<tr><th class="form-inputs">
						<select name="foreign_table" onchange="loadTableFields(this, 'foreign');">
							<option value="">Table</option>
							<?php foreach ($tables as $tbl) : ?>
								<option value="<?=$tbl?>"><?=$tbl?></option>
							<?php endforeach ?>
						</select>
					</th></tr>
				<tr><th class="form-inputs">
						<select name="foreign_field">
							<option value="">Field</option>
						</select>
					</th></tr>
				<tr><th>&nbsp;</th></tr>
				<?php if (empty($foreign)) : ?>
					<tr><th>no related tables</th></tr>
				<?php endif ?>
				<?php foreach ($foreign as $relation) : ?>
					<tr><th><?=$database?>.<?=$relation['foreign_table']?></th></tr>
					<?php foreach($db->getTableColumns($relation['foreign_table']) as $column) : ?>
						<tr><td<?php if ($relation['foreign_field'] === $column['Field']) : ?> class="selected"<?php endif ?><?php if ($column['Key'] === 'PRI') : ?> title="Primary Key"<?php endif ?>>
								<?=$column['Field']?>
							</td></tr>
					<?php endforeach ?>
				<?php endforeach ?>
			</table>
		</div>
		<div class="foreign-links">
			<table class="column-links">
				<tr><td class="form-arrow">&rarr;</td></tr>
				<tr><td>&nbsp;</td></tr>
				<?php foreach ($foreign as $relation) : ?>
					<tr><td>&nbsp;</td></tr>
					<?php foreach($db->getTableColumns($relation['foreign_table']) as $column) : ?>
						<tr>
							<?php if ($relation['foreign_field'] === $column['Field']) : ?>
								<td class="relation-arrow">
									&rarr;
									<a href="" title="Delete relation" onclick="return removeRelation('<?=$relation['foreign_table']?>', '<?=$relation['foreign_field']?>', '<?=$relation['master_table']?>', '<?=$relation['master_field']?>');"><img src="img/delete.gif" width="18" height="18" alt="Delete relation" /></a>
								</td>
							<?php else : ?>
								<td>&nbsp;</td>
							<?php endif ?>
						</tr>
					<?php endforeach ?>
				<?php endforeach ?>
			</table>
		</div>

		<div class="original-table">
			<table class="column-list">
				<tr class="form-inputs"><th class="form-inputs">
						<select name="original_field">
							<?php foreach($columns as $column) : ?>
								<option><?=$column['Field']?></option>
							<?php endforeach ?>
						</select>
						<input name="original_table" type="hidden" value="<?=$table?>" />
						<input name="master_db" type="hidden" value="<?=$database?>" />
						<input name="foreign_db" type="hidden" value="<?=$database?>" />
					</th></tr>
				<tr><th class="form-inputs">
						<input type="submit" value="Add Relation" />
					</th></tr>
				<tr><th>&nbsp;</th></tr>
				<tr><th><?=$database?>.<?=$table?></th></tr>
				<?php foreach($columns as $column) : ?>
					<tr><td<?php if (in_array($column['Field'], $relatedFields)) : ?> class="selected"<?php endif ?><?php if ($column['Key'] === 'PRI') : ?> title="Primary Key"<?php endif ?>>
							<?=$column['Field']?>
						</td></tr>
				<?php endforeach ?>
			</table>
		</div>

		<div class="master-links">
			<table class="column-links">
				<tr><td class="form-arrow">&rarr;</td></tr>
				<tr><td>&nbsp;</td></tr>
				<?php foreach ($master as $relation) : ?>
					<tr><td>&nbsp;</td></tr>
					<?php foreach($db->getTableColumns($relation['master_table']) as $column) : ?>
						<tr>
							<?php if ($relation['master_field'] === $column['Field']) : ?>
								<td class="relation-arrow">
									&rarr;
									<a href="" title="Delete relation" onclick="return removeRelation('<?=$relation['foreign_table']?>', '<?=$relation['foreign_field']?>', '<?=$relation['master_table']?>', '<?=$relation['master_field']?>');"><img src="img/delete.gif" width="18" height="18" alt="Delete relation" /></a>
								</td>
							<?php else : ?>
								<td>&nbsp;</td>
							<?php endif ?>
						</tr>
					<?php endforeach ?>
				<?php endforeach ?>
			</table>
		</div>

		<div class="master-tables">
			<table class="column-list">
				<tr><th class="form-inputs">
						<select name="master_table" onchange="loadTableFields(this, 'master');">
							<option>Table</option>
							<?php foreach ($tables as $tbl) : ?>
								<option value="<?=$tbl?>"><?=$tbl?></option>
							<?php endforeach ?>
						</select>
					</th></tr>
				<tr><th class="form-inputs">
						<select name="master_field">
							<option>Field</option>
						</select>
					</th></tr>
				<tr><th>&nbsp;</th></tr>
				<?php if (empty($master)) : ?>
					<tr><th>no related tables</th></tr>
				<?php endif ?>
				<?php foreach ($master as $relation) : ?>
					<tr><th><?=$database?>.<?=$relation['master_table']?></th></tr>
					<?php foreach($db->getTableColumns($relation['master_table']) as $column) : ?>
						<tr><td<?php if ($relation['master_field'] === $column['Field']) : ?> class="selected"<?php endif?><?php if ($column['Key'] === 'PRI') : ?> title="Primary Key"<?php endif ?>>
								<?=$column['Field']?>
							</td></tr>
					<?php endforeach ?>
				<?php endforeach ?>
			</table>
		</div>
		<div class="detector-link">
			<em>or</em>
			<a href="detect.php?database=<?=$database?>">Detect table relations automatically</a>
		</div>
	</form>
	<form action="<?=$_SERVER['REQUEST_URI']?>" method="post" name="remove_relation_form">
		<input type="hidden" name="foreign_db" value="<?=$database?>" />
		<input type="hidden" name="foreign_table" value="" />
		<input type="hidden" name="foreign_field" value="" />
		<input type="hidden" name="master_db" value="<?=$database?>" />
		<input type="hidden" name="master_table" value="" />
		<input type="hidden" name="master_field" value="" />
		<input type="hidden" name="remove_relation" value="true" />
	</form>
	<script src='img/request.js'></script>
	<script src='img/settings.js'></script>
<?php require_once 'inc/footer.php'; ?>