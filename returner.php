<?php
require 'lib/core.php';
$dbr = new DbrBaseUtils($_GET);
if (isset($_GET['table'])) $result = array_keys($dbr->getTableColumns($_GET['table']));
if (isset($_GET['database'])) $result = $dbr->getDatabaseTables();
die(implode(',', $result));