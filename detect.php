<?php
require_once 'lib/core.php';
require_once 'lib/detector.php';
require_once 'lib/manager.php';
require_once 'inc/header.php';

$detector = new tableRelationDetector($_GET['database']);
$databases = $detector->db->fetch('show databases');

if (isset($_POST['relations'])) {
	$relationManager = new TableRelationManager();
	$relationManager->resetRelations(array_filter($_POST['relations'], function($item) {
		return isset($item['selected']) and $item['selected'] === 'true';
	}), $_GET['database']);
}
if (isset($_GET['database'])) {
	$relations = $detector->detectTableRelations($detector->database);
}
?>
<h1>Find table relations in<?php if (isset($detector)) : ?> <a href="./?database=<?=$detector->database?>"><?=$detector->database?></a><?php endif ?>:</h1>
<form action="<?=$_SERVER['PHP_SELF']?>">
	<select name="database" class="database-selector">
	<?php foreach ($databases as $database) : ?>
		<option value="<?=$database?>"<?=($database == $detector->database ? ' selected' : '' )?>><?=$database?></option>
	<?php endforeach ?>
	</select>
	<input type="submit" value="Find" />
</form>
<?php if (isset($relations)) : ?>
	<form action="<?=$_SERVER['PHP_SELF']?>?database=<?=$detector->database?>" method="post" name="relation_detector_form">
	<table class="data">
		<tr class="blank-heading">
			<td colspan="4"><h2>Suggested relations:</h2></td>
		</tr>
		<?php if (!empty($relations)) : ?>
			<tr>
				<th><input type="checkbox" data-type="new" onclick="return toggleCheckbox(this);" /></th>
				<th>Primary</th>
				<th>&nbsp;</th>
				<th>Related</th>
			</tr>
		<?php else : ?>
			<tr class="clear"><td colspan="4">No relation suggestions found</td></tr>
		<?php endif ?>
		<?php
		$key = 0;
		foreach ($relations as $relation) :	?>
			<tr class="clear relations-high"><td>
					<input type="checkbox" name="relations[<?=$key?>][selected]" value="true" data-type="new" id="relation-<?=$key?>" />
					<input type="hidden" name="relations[<?=$key?>][foreign_db]" value="<?=$detector->database?>" />
					<input type="hidden" name="relations[<?=$key?>][foreign_table]" value="<?=$relation['foreign_table']?>" />
					<input type="hidden" name="relations[<?=$key?>][foreign_field]" value="<?=$relation['foreign_field']?>" />
					<input type="hidden" name="relations[<?=$key?>][master_db]" value="<?=$detector->database?>" />
					<input type="hidden" name="relations[<?=$key?>][master_table]" value="<?=$relation['master_table']?>" />
					<input type="hidden" name="relations[<?=$key?>][master_field]" value="<?=$relation['master_field']?>" />
				</td><td>
					<label for="relation<?=$key?>"><?=$relation['foreign_table']?>.<?=$relation['foreign_field']?></label>
				</td><td>
					<label for="relation<?=$key?>">&rarr;</label>
				</td><td>
					<label for="relation<?=$key?>"><?=$relation['master_table']?>.<?=$relation['master_field']?></label>
				</td></tr>
			<?php
			$key++;
		endforeach
		?>
		<?php if (!empty($detector->existing)) : ?>
			<tr class="blank-heading">
				<td colspan="4"><h2>Existing relations:</h2></td>
			</tr>
			<tr>
				<th><input type="checkbox" checked="checked" data-type="existing" onclick="return toggleCheckbox(this);" /></th>
				<th>Primary</th>
				<th>&nbsp;</th>
				<th>Related</th>
			</tr>
		<?php endif ?>
		<?php
		foreach ($detector->existing as $relation) :
		?>
			<tr class="clear relations-high"><td>
					<input type="checkbox" name="relations[<?=$key?>][selected]" value="true" data-type="existing" id="relation-<?=$key?>" checked="checked" />
					<input type="hidden" name="relations[<?=$key?>][foreign_db]" value="<?=$detector->database?>" />
					<input type="hidden" name="relations[<?=$key?>][foreign_table]" value="<?=$relation['foreign_table']?>" />
					<input type="hidden" name="relations[<?=$key?>][foreign_field]" value="<?=$relation['foreign_field']?>" />
					<input type="hidden" name="relations[<?=$key?>][master_db]" value="<?=$detector->database?>" />
					<input type="hidden" name="relations[<?=$key?>][master_table]" value="<?=$relation['master_table']?>" />
					<input type="hidden" name="relations[<?=$key?>][master_field]" value="<?=$relation['master_field']?>" />
				</td><td>
					<label for="relation<?=$key?>"><?=$relation['foreign_table']?>.<?=$relation['foreign_field']?></label>
				</td><td>
					<label for="relation<?=$key?>">&rarr;</label>
				</td><td>
					<label for="relation<?=$key?>"><?=$relation['master_table']?>.<?=$relation['master_field']?></label>
				</td></tr>
		<?php
			$key++;
		endforeach
		?>
	</table>
	<br />
	<input type="submit" value="Save Changes" disabled="disabled" name="detector_submit" />
	</form>
<?php endif ?>
<script src='img/detect.js'></script>
<?php require_once 'inc/footer.php'; ?>