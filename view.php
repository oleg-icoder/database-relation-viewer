<?php
require_once 'lib/core.php';
require_once 'lib/viewer.php';
require_once 'lib/action.php';
new TableAction();

$table = new TableView($_GET['database'], $_GET['table']);
$data		=& $table->data;
$children	=& $table->children;
$parents	=& $table->parents;
if (!empty($_GET['table'])) {
	$tableVisits = 1;
	$tableKey = $table->database.'.'.$table->name;
	if (isset($_COOKIE['dbrTableVisit'][$tableKey])) $tableVisits = $_COOKIE['dbrTableVisit'][$tableKey] + 1;

	setcookie('dbrTableVisit['.$tableKey.']', $tableVisits, strtotime('+1 year'));
}

require_once 'inc/header.php';
?>
<div id="edit">
	<div class="arrow"></div>
	<div class="main">
		<form action="<?=$_SERVER['PHP_SELF']?>" name="editForm">
			<input type="text" name="edit[value]" class="text" />
			<input type="hidden" name="edit[backUrl]" value="<?=$_SERVER['REQUEST_URI']?>" />
			<input type="image" src="img/edit.gif" width="18" height="18" onclick="return document.forms['editForm'].submit();" title="Edit column value" />
		</form>
	</div>
</div>
<?php if (!empty($table->relations)) : ?>
<div id="tooltip">
	<div class="arrow"></div>
	<div class="main">
		<table class="data">
		<tr><th>
			status
		</th><td>
			Loading...
		</td></tr>
		</table>
	</div>
</div>
<?php endif ?>

<h1 class="selected-table">
	<a href="./?database=<?=$table->database?>" title="Select database"><?=$table->database?></a>.<a href="view.php?database=<?=$table->database?>&table=<?=$table->name?>" title="View table data"><?=$table->name?></a>
	&nbsp;<a href="settings.php?database=<?=$table->database?>&table=<?=$table->name?>"><img src="img/share.svg" width="28" height="28" title="Manage table relations" /></a>
</h1>
<?php require 'inc/table.php'; ?>

<?php
if (empty($table->data)) {
	require_once 'inc/footer.php';
	exit;
}
?>

<?php
$current = $table->data[0];
if (count($table->data) != 1) {
	$children = array();
	$parents = array();
}

foreach ($parents as $parent) :
	$data = $table->getRelatedData($parent['foreign_db'], $parent['foreign_table'], $parent['foreign_field'], $current[$parent['master_field']]);
	if (empty($data))  continue;

	$table = new tableView($parent['foreign_db'], $parent['foreign_table'], false);
?>
	<h2 class="parent"><a href="view.php?database=<?=$parent['foreign_db']?>&table=<?=$parent['foreign_table']?>" title="View table data"><?=$parent['foreign_table']?></a></h2>
	<?php require 'inc/table.php'; ?>
<?php endforeach ?>

<?php
foreach ($children as $child) :
	$data = $table->getRelatedData($child['master_db'], $child['master_table'], $child['master_field'], $current[$child['foreign_field']]);
	if (empty($data))  continue;

	$table = new tableView($child['master_db'], $child['master_table'], false);
?>
	<h2 class="child">
		<a href="view.php?database=<?=$child['master_db']?>&table=<?=$child['master_table']?>&search%5B<?=$child['master_field']?>%5D=<?=$current[$child['foreign_field']]?>"  title="View all related records">
			<?=$child['master_table']?>
			<?php if (count($data) >= 5) : ?>
			&larr; click to see more records
			<?php endif ?>
		</a>
	</h2>
	<?php require 'inc/table.php'; ?>
<?php endforeach ?>
<script src="img/request.js"></script>
<script src="img/table.js"></script>
<?php require_once 'inc/footer.php'; ?>