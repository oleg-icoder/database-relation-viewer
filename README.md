# README #

DBR - Database Relation (viewer) is PHP tool to quickly browse through table data with fields (keys) relations.
Currently all DB relations are stored at phpmyadmin.pma_relation table as historical background of phpMyAdmin tool

### What is this repository for? ###

We are using this repo to contribute development and clone DBR in DEVBOX

### How do I get set up? ###

All adjustable settings can be changed at /lib/core.php