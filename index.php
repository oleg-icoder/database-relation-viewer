<?php
require_once 'lib/core.php';
$dbr = new DbrBaseUtils($_GET);
$databases = $dbr->getDatabases();
if (empty($dbr->database)) $dbr->db->selectDb(current($databases));
$tables = $dbr->getDatabaseTables();
require_once 'inc/header.php';

$preselect = isset($_COOKIE['dbrTableVisit']) ? $_COOKIE['dbrTableVisit'] : array();
arsort($preselect);
?>
<div class="selForm">
	<form action="view.php" name="dbSelect" onSubmit="return selectInput(this);">
	<ul><li>
		<h1>Previous:</h1>
		<select name="preselect" id="preselect" multiple="multiple" ondblclick="selectTable(this);">
			<?php
			foreach ($preselect as $key => $visits) :
				list($database, $table) = explode('.', $key);
				if (empty($table) or empty($database)) continue;
			?>
				<option value="<?=$database?>.<?=$table?>"><?=$table?>.<?=$database?></option>
			<?php endforeach ?>
		</select>
	</li><li class="middle">
		<b>or</b>
	</li><li>
		<h1 class="database-header">Database:</h1>
		<select name="database" multiple="multiple">
		<?php foreach ($databases as $database) : ?>
			<option value="<?=$database?>"<?=($database == $dbr->database ? ' selected' : '' )?>><?=$database?></option>
		<?php endforeach ?>
		</select>
	</li><li class="middle">
		<b>&rarr;</b>
	</li><li>
		<h1>Table:</h1>
		<select name="table" multiple="multiple" ondblclick="return selectInput(this.form);">
		<?php foreach ($tables as $table) : ?>
		<option value="<?=$table?>"><?=$table?></option>
		<?php endforeach ?>
		</select>
	</li></ul>
	<div class="db-submit"><input type="submit" value="Select Table"></div>
	</form>
</div>

<script src='img/request.js'></script>
<script src='img/select.js'></script>
<script>
	function selectTable(input) {
		var dist = input.value.split('.')
		document.location.href = 'view.php?database='+ dist[0] + '&table=' + dist[1];
		return false;
	}
	function selectInput(form) {
		var preselect = document.getElementById('preselect');
		if (preselect.value != '' && document.activeElement != form.table) {
			return selectTable(preselect);
		}
		if (form.table.value == '') {
			alert("Table is not selected.")
			form.table.focus();
			return false;
		}
		form.submit();
		return false;
	}
</script>

<?php require_once 'inc/footer.php'; ?>