<form action="<?=$_SERVER['PHP_SELF']?>" onsubmit="return initSearch(this);">
<table class="data">
<tr>
<?php foreach ($table->columns as $column) : ?>
	<?php if ($column['OrderedBy']) : ?>
		<th class="selected <?=($table->getDirection() == 'asc' ? 'asc' : 'desc')?>">
	<?php else : ?>
		<th>
	<?php endif ?>
	<a href="<?=$_SERVER['REQUEST_URI']?>&order%5Bfield%5D=<?=$column['Field']?>&order%5Bdirection%5D=<?=$table->getDirection();?>" title="Sort by <?=$column['Field']?> column"><?=$column['Field']?></a></th>
<?php endforeach ?>
<th colspan="2">actions</th>
</tr>
<?php if ((sizeof($data) > 5 or isset($_GET['search'])) and $_GET['table'] === $table->name and !isset($parent)) : ?>
<tr>
	<?php foreach ($table->columns as $column) : ?>
		<td class="search"><input name="search[<?=$column['Field']?>]" value="<?=(isset($_GET['search'][$column['Field']]) ? $_GET['search'][$column['Field']] : '')?>" class="search" /></td>
	<?php endforeach ?>
	<td>
		<input type="hidden" name="database" value="<?=$_GET['database']?>" />
		<input type="hidden" name="table" value="<?=$_GET['table']?>" />
		<input type="submit" value="search" />
	</td>
</tr>
<?php endif ?>
<?php foreach ($data as $row) : ?>
	<tr>
		<?php foreach ($row as $field => $value) : ?>
			<?php if (isset($table->relations[$field]) && !empty($row[$field])) : ?>
				<td><a href="view.php?database=<?=$table->relations[$field]['db']?>&table=<?=$table->relations[$field]['table']?>&search%5B<?=$table->relations[$field]['field']?>%5D=<?=$row[$field]?>" data-relation="<?=($table->hasTooltip($table->relations[$field]['table'], $table->relations[$field]['field']) ? 'tooltip' : 'false')?>"><?=$value?></a></td>
			<?php else : ?>
				<?php if ($value === null) : ?>
					<td class="centered">NULL</td>
				<?php elseif ($table->columns[$field]['Type'] == 'float') : ?>
					<td class="rightend"><?=$value?></td>
				<?php elseif ($table->columns[$field]['Type'] == 'int') : ?>
					<td class="centered"><?=$value?></td>
				<?php elseif ($value == '') : ?>
					<td class="centered" title="Empty string">&mdash;</td>
				<?php else : ?>
					<td class="textend"><?=htmlentities($value)?></td>
				<?php endif ?>
			<?php endif ?>
		<?php endforeach ?>
		<!--<td class="action"><a href="edit.php?edit%5Bdatabase%5D=<?=$table->database?>&edit%5Btable%5D=<?=$table->name?>&edit%5Bkey%5D=<?=$table->primaryKey?>&edit%5Bid%5D=<?=urlencode($row[$table->primaryKey])?>"><img src="img/edit.gif" width="18" height="18" title="Edit row" alt="Edit row" /></a></td>-->
		<td class="action"><a href="<?=$_SERVER['REQUEST_URI']?>&remove%5Bdatabase%5D=<?=$table->database?>&remove%5Btable%5D=<?=$table->name?>&remove%5Bkey%5D=<?=$table->primaryKey?>&remove%5Bid%5D=<?=urlencode($row[$table->primaryKey])?>" onClick="return remove(this);"><img src="img/delete.gif" width="18" height="18" title="Delete row" alt="Delete row" /></a></td>
	</tr>
<?php endforeach ?>
</table>
</form>