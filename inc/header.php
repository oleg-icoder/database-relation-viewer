<?php
	if (!isset($styles)) $styles = array('img/rel.css');
	$styles = array_unique(array_merge(array('img/common.css'), $styles));
?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
	<?php if (isset($_GET['table'])) : ?>
	<title><?=$_GET['database']?>.<?=$_GET['table']?></title>
	<?php else : ?>
	<title>DB Relations Browser</title>
	<?php endif ?>
	<?php foreach ($styles as $styleUrl) : ?>
	<link rel="stylesheet" href="<?=$styleUrl?>" type="text/css" />
	<?php endforeach ?>
	<link rel="shortcut icon" href="favicon.ico" />
</head>
<body>