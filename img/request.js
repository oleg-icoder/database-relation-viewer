function getReqObject() {
	var xmlhttp;
	try {
		xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} catch (e) {
		try {
			xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
		} catch (E) {
			xmlhttp = false;
		}
	}
	if (!xmlhttp && typeof XMLHttpRequest != 'undefined') xmlhttp = new XMLHttpRequest();
	return xmlhttp;
}
function getRequest(url, callback) {
	var req = getReqObject();
	req.onreadystatechange = function() {
		if (req.readyState != 4) return false;
		callback(req);
	}
	req.open("GET", url);
	req.send(null);
}