var tip, edit, parents, timer, editTimer;
		
function setTooltipContents(contents) {
	var div = tip.getElementsByTagName('div')[1];
	div.innerHTML = contents;
	return true;
}
function showTooltip(link) {
	if (link.getAttribute('data-relation') != 'tooltip') return false;
	var url = link.getAttribute('href');
	url = 'tooltip.php' + url.substr(url.indexOf('?'), url.length);
	getRequest(url, function(req) { setTooltipContents(req.responseText); });

	var position = getElementPosition(link);
	console.log(link);
	console.log(position);
	tip.style.left = position.left - 0 + 'px';
	tip.style.top = position.top + 20 + 'px';
	tip.style.display = 'block';

	return true;
}
function hideTooltip() {
	clearTimeout(timer);
	timer = setTimeout(function() { tip.style.display = 'none'; }, 1000);
}
function remove(obj) {
	var message = 'You want to remove this row?';
	return confirm(message);
}

window.onload = function() {
	tip = document.getElementById('tooltip');
	var parents = document.getElementsByTagName('a');
	if (!parents) return false;

	for (var i = 0; i <= parents.length - 1; i++) {
		parents[i].onmouseover = function(e) {
			var link = window.event ? window.event.srcElement : e.target;
			clearTimeout(timer);
			timer = setTimeout(function() {
				showTooltip(link);
			}, 200);
		}
		parents[i].onmouseout = function() {
			hideTooltip();
		}
		tip.onmouseover = function() {
			tip.style.display = 'block';
		}
		tip.onmouseout = function() {
			hideTooltip();
		}
	}

	edit = document.getElementById('edit');
	var parents = document.getElementsByTagName('td');
	if (!parents) return false;
	for (var i = 0; i <= parents.length - 1; i++) {
		if (!checkTableCell(parents[i])) continue;
		parents[i].onclick = function(e) {
			var cell = (window.event) ? window.event.srcElement : e.target;
			if (checkTag(cell, 'a')) return true;
			if (!checkTag(cell, 'td')) cell = cell.parentNode;
			showEdit(cell);
		}
	}
	edit.onmouseover = function() {
		clearTimeout(editTimer);
		edit.style.display = 'block';
	};
	edit.onmouseout = function() {
		editTimer = setTimeout(function () {
			edit.style.display = 'none';
		}, 2000);
	};
}
function getElementPosition(obj) {
    var left = 0;
    var top = 0;
    while(obj) {
        left += obj.offsetLeft;
        top += obj.offsetTop;
        obj = obj.offsetParent;
    }
    return {'left':left, 'top':top};
}
function checkTag(obj, tag) {
	if (obj.nodeName.toLowerCase() != tag) return false;
	return true;
}
function checkTableCell(cell) {
	if (cell.className === 'search' || cell.className === 'action') return false;
	var row = cell.parentNode;
	if (!checkTag(row, 'tr')) return false;
	if (row.className) return false;
	var table = row.parentNode.parentNode;
	if (!checkTag(table, 'table')) return false;
	if (table.className.indexOf('data') < 0) return false;
	return true;
}
function getColumnName(cell) {
	if (!checkTag(cell, 'td')) return false;
	var row = cell.parentNode;
	if (!checkTag(row, 'tr')) return false;
	var table = row.parentNode.parentNode;
	if (!checkTag(table, 'table')) return false;

	var columns = table.getElementsByTagName('th');
	var values = new Array();
	for (var i = 0; i <= columns.length - 1; i++) {
		values.push(getColumnValue(columns[i]));
	}
	columns = row.getElementsByTagName('td');
	for (var i = 0; i <= columns.length - 1; i++) {
		if (columns[i] == cell) values = values[i];
	}
	return values;
}
function getColumnValue(cell) {
	if (cell.innerHTML.indexOf('<') < 0) {
		var value = cell.innerHTML;
	} else {
		if (cell.firstChild.innerHTML) {
			var value = cell.firstChild.innerHTML
		} else {
			var value = cell.firstChild.nextSibling.innerHTML
		}
	}
	if (value == '�') return '';
	return value;
}
function getColumnAction(cell) {
	if (cell.className && cell.className == 'action') return cell.firstChild.href;
	return getColumnAction(cell.nextSibling);
}
function fillColumnForm(cell) {
	var url = getColumnAction(cell);
	url = url.substr(url.lastIndexOf('?') + 1, url.length);
	url = url.split('&');
	var fields = new Array();
	for (i = 0; i <= url.length - 1; i++) {
		var pair = url[i].split('=');
		pair[0] = pair[0].replace('%5B', '[');
		pair[0] = pair[0].replace('%5D', ']');
		fields.push(pair);
	}
	fields.push( new Array('edit[column]', getColumnName(cell)) );

	var form = edit.getElementsByTagName('form')[0];
	form.elements['edit[value]'].value = getColumnValue(cell);
	for(var i = 0; i <= fields.length - 1; i++) {
		var input = document.createElement('input');
		input.setAttribute('type', 'hidden');
		input.setAttribute('name', fields[i][0]);
		input.setAttribute('value', fields[i][1]);
		form.appendChild(input);
	}
	form.elements['edit[value]'].select();
	form.elements['edit[value]'].focus();
	return true;
}
function showEdit(cell) {
	var position = getElementPosition(cell);
	edit.style.width = (cell.offsetWidth + 20) + 'px';
	edit.style.left = (position.left - 8) + 'px';
	edit.style.top = (position.top + 20) + 'px';
	edit.style.display = 'block';
	
	var form = document.forms['editForm'];
	fillColumnForm(cell);

	clearTimeout(editTimer);
	editTimer = setTimeout("edit.style.display = 'none'", 3000);
	form.elements['edit[value]'].onkeyup = function() {
		clearTimeout(editTimer);
	};
	form.elements['edit[value]'].onblur = function() {
		editTimer = setTimeout("edit.style.display = 'none'", 200);
	};
	//alert(form.elements['edit[column]'].value);
}

function initSearch(form) {
    if (!form) return false;
    for (var i = 0; i < form.elements.length; i++) {
        if (form.elements[i].name.indexOf('search[') == -1) continue;
        if (form.elements[i].value != '') continue;
        form.elements[i].disabled = true;
    }
    return true;
}