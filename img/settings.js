function setColumnList(columns, list) {
	columns = columns.split(',');
	clearSelectOptions(list);
	for (var i = 0; i < columns.length; i++) {
		var option = document.createElement('option');
		option.value = columns[i];
		option.innerHTML = columns[i];
		list.appendChild(option);
	}
}
function clearSelectOptions(list) {
	while (list.options.length > 0) {
		list.remove(0);
	}
}
function loadTableFields(table, type) {
	var form = document.forms['add_relation_form'];
	var list = form.elements[type + '_field'];
	if (table.value.trim() === '') {
		clearSelectOptions(list);
		return false;
	}
	var antiType = type === 'master' ? 'foreign' : 'master';
	var otherList = form.elements[antiType + '_field'];
	clearSelectOptions(otherList);
	form.elements[antiType + '_table'].options[0].selected = true;
	var url = 'returner.php?database=<?=$database?>&table=' + table.value;
	getRequest(url, function(req) { setColumnList(req.responseText, list); } );
}
function removeRelation(foreign_table, foreign_field, master_table, master_field) {
	var message = 'Remove this relation';
	if (!confirm(message)) return false;

	var form = document.forms['remove_relation_form'];
	form.elements['foreign_table'].value = foreign_table;
	form.elements['foreign_field'].value = foreign_field;
	form.elements['master_table'].value = master_table;
	form.elements['master_field'].value = master_field;
	form.submit();
	return false;
}
