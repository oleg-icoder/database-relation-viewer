function toggleCheckbox(box) {
	var elements = box.form.elements;
	for (var i = 0; i < elements.length; i++) {
		if (elements[i].type != 'checkbox') continue;
		if (elements[i].getAttribute('data-type') != box.getAttribute('data-type')) continue;
		if (elements[i] == box) continue;
		elements[i].checked = box.checked;
	}
}
document.addEventListener('DOMContentLoaded', function(){
	var list = document.forms['relation_detector_form'].elements;
	for (var i = 0; i < list.length; i++) {
		if (list[i].type != 'checkbox') continue;

		list[i].addEventListener("click", function() {
			document.forms['relation_detector_form'].elements['detector_submit'].disabled = false;
		});
	}
}, false);