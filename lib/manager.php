<?php
class TableRelationManager extends DbrBaseUtils {
	private $fields = array('master_db', 'master_table', 'master_field', 'foreign_db', 'foreign_table', 'foreign_field');
	public function __construct() {
		parent::__construct($_GET);
	}
	public function removeRelation($relation) {
		$query = "delete from {$this->relationsDb}.{$this->relationsTable}
		where foreign_db = '{$relation['foreign_db']}'
		and foreign_table = '{$relation['foreign_table']}'
		and foreign_field = '{$relation['foreign_field']}'
		and master_db = '{$relation['master_db']}'
		and master_table = '{$relation['master_table']}'
		and master_field = '{$relation['master_field']}'";
		return $this->query($query);
	}
	public function addRelation($relation) {
		if (empty($relation['master_field'])) {
			$relation['master_table'] = $relation['original_table'];
			$relation['master_field'] = $relation['original_field'];
		} else {
			$relation['foreign_table'] = $relation['original_table'];
			$relation['foreign_field'] = $relation['original_field'];
		}
		return $this->resetRelations(array($relation), false);
	}
	public function cleanDbRelations($database) {
		if (empty($database)) return false;
		$query = "delete from {$this->relationsDb}.{$this->relationsTable}
		where foreign_db = '{$database}'
		and master_db = foreign_db";
		$this->query($query);
	}
	public function resetRelations($relations, $reset = true) {
		if (empty($relations)) return false;
		if ($reset) {
			$relation = current($relations);
			$this->cleanDbRelations($relation['foreign_db']);
		}

		$query = 'insert into '.$this->relationsDb.'.'.$this->relationsTable.'
		('.implode(', ', $this->fields).') values ';
		$lines = array();
		foreach ($relations as $relation) {
			$values = array();
			foreach ($this->fields as $field) {
				$values[] = isset($relation[$field]) ? $relation[$field] : '';
			}
			$lines[] = "('".implode("', '", $values)."')";
		}
		$query .= implode(', ', $lines);
		return $this->query($query);
	}
}