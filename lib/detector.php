<?php
class TableRelationDetector extends DbrBaseUtils {
	public $tables, $columns, $existing = array();
	private $rowsAccuracy = 0.2;

	public function __construct($database) {
		parent::__construct(compact('database'));
		$this->tables = $this->getDatabaseTables();
		$this->columns = array();
		foreach ($this->tables as $table) {
			$this->columns[$table] = $this->getTableColumns($table);
		}
		$this->existing = $this->getExistingRelations($database);
	}
	private function getPrimaryKey($table)	{
		if (!isset($this->columns[$table])) return false;

		$primaryKey = '';
		$keyCount = 0;
		foreach ($this->columns[$table] as $key => $column) {
			if ($column['Key'] === 'PRI') {
				$primaryKey = $column;
				$keyCount++;
			}
		}
		return $keyCount === 1 ? $primaryKey : false;
	}
	public function detectTableRelations($db) {
		$results = array();
		foreach ($this->tables as $table) {
			$primaryKey = $this->getPrimaryKey($table);
			if (empty($primaryKey)) continue;

			$relations = $this->getPossibleRelations($table, $primaryKey['Field'], $primaryKey['Type']);
			if (empty($relations)) continue;

			$results = array_merge($this->filterRelations($db, $table, $primaryKey['Field'], $relations), $results);
		}
		return $results;
	}
	private function getPossibleRelations($srcTable, $srcColumn, $srcType) {
		$results = array();
		foreach ($this->columns as $table => $columns) {
			foreach ($columns as $column) {
				if ($table == $srcTable and $column['Field'] == $srcColumn) continue;
				if ($column['Type'] != $srcType) continue;
				$results[] = array(
					'table' => $table,
					'column' => $column['Field']
				);
			}
		}
		return $results;
	}
	private function isRelationExists($candidate) {
		$fields = array('foreign_table', 'foreign_field', 'master_table', 'master_field');
		$fieldsCount = sizeof($fields);
		foreach ($this->existing as $relation) {
			$matchCount = 0;
			foreach ($fields as $field) {
				if ($relation[$field] === $candidate[$field]) $matchCount++;
			}
			if ($matchCount === $fieldsCount) return true;
		}
		return false;
	}
	private function filterRelations($db, $table, $column, $relations) {
		$query = 'select count('.$column.') from '.$db.'.'.$table;
		$totalRows = $this->db->value($query);
		$query = 'select min('.$column.') as minVal, max('.$column.') as maxVal from '.$db.'.'.$table;
		$range = $this->db->row($query);
		$results = array();
		foreach ($relations as $relation) {
			$candidate = array(
				'foreign_table'	=> $table,
				'foreign_field'	=> $column,
				'master_table'	=> $relation['table'],
				'master_field'	=> $relation['column'],
			);
			if ($this->isRelationExists($candidate)) continue;

			$query = 'select min('.$relation['column'].') as minVal, max('.$relation['column'].') as maxVal
			from '.$db.'.'.$relation['table'];
			$cRange = $this->db->row($query);
			if (!$this->checkRange($range['minVal'], $range['maxVal'], $cRange['minVal'], $cRange['maxVal'])) continue;

			$query = 'select count(distinct foreign_table.'.$column.')
			from '.$db.'.'.$table.' as foreign_table, '.$db.'.'.$relation['table'].' as master_table
			where foreign_table.'.$column.' = master_table.'.$relation['column'].'
			limit 10000';
			$rows = $this->db->value($query);
			if ($rows <= 0 or $rows <= $totalRows * 0.2) continue;

			$results[] = $candidate;
		}
		return $results;
	}
	private function checkRange($primaryMin, $primaryMax, $secondaryMin, $secondaryMax) {
		if (!is_numeric($primaryMin)) return false;
		if ($secondaryMin < $primaryMin or $secondaryMax > $primaryMax) return false;
		return $primaryMin * (1 + $this->rowsAccuracy) > $secondaryMin and $primaryMax * (1 - $this->rowsAccuracy) < $secondaryMax;
	}
}
