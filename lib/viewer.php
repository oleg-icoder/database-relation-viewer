<?php
class TableView extends DbrBaseUtils {
	public $columnTypes = array(
    	'decimal' => 'float',
	    'varchar' => 'text'
	);
	public $name, $columns, $data, $children, $parents, $relations, $primaryKey;

	public function __construct($database, $table, $parent = true) {
		if (empty($table) or empty($database)) return false;
		parent::__construct(compact('database'));

		$this->name = $table;
		$this->columns = $this->getColumns($table);
		if (empty($this->columns)) {
			return $this->err('\''.$_GET['table'].'\' table does not exists.');
		}
		if ($parent) {
            $this->data = $this->getData($table, isset($_GET['search']) ? $_GET['search'] : array());
        }
		$this->children = $this->getRelations($table, 'foreign');
		$this->parents = $this->getRelations($table, 'master');
		$this->relations = $this->formatRelations();
	}
	public function getData($table, $search) {
		if (!is_array($search)) $search = array();
		$query = 'select * from '.$this->database.'.'.$table.' where 1 = 1 ';
		foreach ($search as $field => $value) {
			if (empty($value)) continue;
			if (is_numeric($value)) {
				$query .= 'and '.$this->database.'.'.$table.'.'.$field.' = '.$value.' ';
			} else {
				if (strpos($value, '%') == false) $value .= '%';
				$query .= 'and '.$this->database.'.'.$table.'.'.$field.' like \''.$value.'\' ';
			}
		}
		if (isset($_GET['order']['field'])) {
			$orderField = $_GET['order']['field'];
		} elseif (!empty($this->primaryKey)) {
			$orderField = $this->primaryKey;
		} else {
			$orderField = current($this->columns);
			$orderField = $orderField['Field'];
		}
		$orderDirection = 'desc';
		if (isset($_GET['order']['direction'])) $orderDirection = $_GET['order']['direction'];
		$query .= 'order by '.$this->database.'.'.$table.'.'.$orderField.' '.$orderDirection.' ';
		$query .= 'limit 100';
		$rows = $this->db->fetch($query);
		$data = array();
		foreach ($rows as $row) {
			foreach ($row as $key => $value) {
				if (strlen($value) > 64) $row[$key] = substr($value, 0, 64).'...';
			}
			$data[] = $row;
		}
		return $data;
	}
	public function getRelatedData($db, $table, $field, $value, $limit = 5) {
		$query = 'select * from '.$db.'.'.$table.'
		where '.$db.'.'.$table.'.'.$field.' = "'.$value.'"
		limit '.$limit;
		$rows = $this->db->fetch($query);
		$data = array();
		foreach ($rows as $row) {
			foreach ($row as $key => $value) {
				if (strlen($value) > 64) $row[$key] = substr($value, 0, 64).'...';
			}
			$data[] = $row;
		}
		return $data;
	}
	public function getColumns($table) {
		if (empty($table)) return false;
		$query = 'show columns from '.$this->database.'.'.$table;
		$columns = $this->db->fetch($query);

		$columnKeys = array();
		foreach ($columns as $key => $column) {
			$columnKeys[] = $column['Field'];
			if ($column['Key'] == 'PRI') $this->primaryKey = $column['Field'];
			if (strpos($column['Type'], '(') !== false) {
			    $column['Type'] = substr($column['Type'], 0, strpos($column['Type'], '('));
			    if (array_key_exists($column['Type'], $this->columnTypes)) $column['Type'] = $this->columnTypes[$column['Type']];
            }
            $column['Search'] = isset($_GET['search'][$column['Field']]) ? $_GET['search'][$column['Field']] : '';
            $column['OrderedBy'] = isset($_GET['order']['field']) && $_GET['order']['field'] == $column['Field'];
            $columns[$key] = $column;
		}
		if (empty($this->primaryKey)) {
			//$column = current($columns);
			$this->primaryKey = false;
		}
		return $this->combineArrays($columnKeys, $columns);
	}
	private function combineArrays(&$keys, &$values) {
		$out = array();
		for($i = 0; $i <= count($keys) - 1; $i++) {
			$out[strval($keys[$i])] = $values[$i];
		}
		return $out;
	}
	public function getRelations($table, $relation) {
		if (empty($table) || empty($relation)) return false;
		$relation_db = 'relation_viewer';
		$relation_table = 'relations';

		$query = 'select master_db, master_table, master_field, foreign_db, foreign_table, foreign_field
		from '.$relation_db.'.'.$relation_table.'
		where '.$relation.'_table = \''.$table.'\'';
		$relations = $this->db->fetch($query);
		if (!is_array($relations)) return array();
		return $relations;
	}
	private function formatRelations() {
		$relations = array();
		foreach ($this->children as $child) {
			$relations[$child['foreign_field']] = array(
				'db'	=> $child['foreign_db'],
				'table' => $child['foreign_table'],
				'field' => $child['foreign_field']
			);
		}
		foreach ($this->parents as $parent) {
			$relations[$parent['master_field']] = array(
				'db'	=> $parent['foreign_db'],
				'table' => $parent['foreign_table'],
				'field' => $parent['foreign_field']
			);
		}
		return $relations;
	}
	public function getDirection() {
		if (!isset($_GET['order']['direction'])) return 'asc';
		$direction = $_GET['order']['direction'];
		if ($direction == 'asc') return 'desc';
		return 'asc';
	}
	public function hasTooltip($table, $field = '') {
		return true;
	}
}