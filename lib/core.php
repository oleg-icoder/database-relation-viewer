<?php
ini_set('display_errors', 1);
//error_reporting(E_ALL);
session_start();

class DbrBaseUtils {
	protected $relationsDb = 'phpmyadmin';
	protected $relationsTable = 'pma_relation';
	public $db;
	public $database = '';

	public function __construct($env) {
		require_once 'mysqlDriver.php';
		$this->db = new DbrMySqlFactory(isset($env['database']) ? $env['database'] : '');
		$this->database = $this->db->database;
		//if ($this->tbx === false) $this->auth();
	}
	public function auth() {
		if (isset($_SERVER['PHP_AUTH_USER']) and $this->db->checkCredentials($_SERVER['PHP_AUTH_USER'], $_SERVER['PHP_AUTH_PW'])) {
		} else {
			header('WWW-Authenticate: Basic realm="DBR Authentication"');
			header('HTTP/1.0 401 Unauthorized');
			die('Please authenticate with DB user credentials.');
		}
	}
	public static function err($err, $backTrace = false) {
		if (empty($err)) return false;
		if ($backTrace) {
			$trace = debug_backtrace();
			$trace = $trace[count($trace) - 1];
			$err_html = '<br><p>' . $err . '<br><br>File: ' . $trace['file'] . ' (' . $trace['line'] . ')<br>Arg: ';
			foreach ($trace['args'] as $key => $arg) {
				if (strlen($arg) > 44) $arg = substr($arg, 0, 42) . '...';
				$err_html .= '[' . $key . '] = [' . $arg . '] ';
			}
			$err_html .= '</p><br>';
		} else {
			$err_html = '<p>'.$err.'</p>';
		}
		die($err_html);
	}
	public function getDatabaseTables($database = '') {
		if (empty($database)) $database = $this->db->database;
		return $this->db->fetch('show tables from '.$database);
	}
	public function getExistingRelations($db, $table = '') {
		$query = "select foreign_table, foreign_field, master_table, master_field
		from {$this->relationsDb}.{$this->relationsTable}
		where foreign_db = '{$db}'
		and master_db = foreign_db";
		if (!empty($table)) $query .= " and (foreign_table = '{$table}' or master_table = '{$table}')";
		return $this->db->fetch($query);
	}
	public function getTableColumns($table) {
		$query = 'show columns from '.$this->database.'.'.$table;
		$columns = $this->db->fetch($query);
		$result = array();
		foreach ($columns as $column) {
			$result[$column['Field']] = $column;
		}
		return $result;
	}
	public function getDatabases() {
		$restricted = array('mysql', 'information_schema', 'performance_schema');
		$result = $this->db->fetch('show databases');
		foreach ($result as $key => $database) {
			if (in_array($database, $restricted)) unset($result[$key]);
		}
		return $result;
	}
}