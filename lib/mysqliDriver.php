<?php
class DbrMySqliFactory {
	private $connection;
	private $host = 'localhost';
	private $user = 'root';
	private $password = '';
	public $database = '';
	public $tbx = false;
	public function __construct($database = '') {
		$this->connect($database);
	}
	public function connect($database = '') {
		$this->connection = mysqli_connect($this->host, $this->user, $this->password);
		if (mysqli_connect_error()) {
			$message = 'Error connecting to host <strong>'.$this->host.'</strong> user <strong>'.$this->user.'</strong> ';
			$message .= '(error code ' . mysqli_connect_errno() . '):<br /> '. mysqli_connect_error();
			return DbrBaseUtils::err($message);
		}
		if (empty($database)) return $this->connection;
		if (!$this->selectDb($database))  {
			DbrBaseUtils::err('Can not select database. '.mysqli_error($this->connection));
			return false;
		}
		return $this->connection;
	}
	public function selectDb($database) {
		if (!mysqli_select_db($this->connection, $database)) return false;
		$this->database = $database;
		return true;
	}
	public function query($query) {
		if (empty($query)) return false;
		if (empty($this->connection)) $this->connect();
		$res = mysqli_query($this->connection, $query);
		if($res == false) {
			DbrBaseUtils::err(mysqli_error($this->connection).'<br><br> Query:<br>'.$query);
			return false;
		}
		return $res;
	}
	public function fetch($query) {
		$res = $this->query($query);
		if (mysqli_num_rows($res) == 0) return array();
		$rows = array();
		while ($row = mysqli_fetch_assoc($res)) {
			if (count($row) == 1) $row = current($row);
			$rows[] = $row;
		}
		mysqli_free_result($res);
		return $rows;
	}
	public function row($query) {
		return current($this->fetch($query));
	}
	public function value($query) {
		$result = $this->fetch($query);
		return is_array($result) ? current($result) : $result;
	}
	public function close() {
		if (empty($this->connection)) return false;
		return mysqli_close($this->connection);
	}
	public function checkCredentials($user, $password) {
		return $user === $this->user and $password === $this->password;
	}
}
